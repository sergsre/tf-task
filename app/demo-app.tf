variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "vpc" {
  default = "DEMO"
}

data "aws_vpc" "selected" {
  tags {
    Name = "${var.vpc}"
  }
}

variable "subn" {
  default = "DEMO Public Subnet 2"
}

data "aws_subnet" "selected" {
  tags {
    Name = "${var.subn}"
  }
}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_s3_bucket" "b" {
  bucket = "kss-tf-demo-bucket"
  acl    = "private"

  tags = {
    Name = "nginx-configuration"
  }
}

resource "aws_security_group" "sg-ec2" {
  name        = "SG-EC2"
  description = "Allow traffic to EC2"
  vpc_id      = "${data.aws_vpc.selected.id}"

  ingress {
    description = "Allow inbound SSH traffic from any IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description     = "Allow all from LB"
    from_port       = 0
    protocol        = "-1"
    to_port         = 0
    security_groups = ["${aws_security_group.sg-elb.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SG-EC2"
  }
}

resource "aws_security_group" "sg-elb" {
  name        = "SG-ELB"
  description = "Allow traffic to ELB"
  vpc_id      = "${data.aws_vpc.selected.id}"

  ingress {
    description = "Allow inbound SSH traffic from any IP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SG-ELB"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"

  vpc_security_group_ids      = ["${aws_security_group.sg-ec2.id}"]
  key_name                    = "nvirginia"
  associate_public_ip_address = "true"
  subnet_id                   = "${data.aws_subnet.selected.id}"

  tags = {
    Name = "demo-app-01"
  }
}

resource "aws_elb" "clb" {
  name = "demo-app-01"

  //  availability_zones = ["us-east-1a", "us-east-1b"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
  instances                   = ["${aws_instance.web.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups             = ["${aws_security_group.sg-elb.id}"]
  subnets                     = ["${data.aws_subnet.selected.id}"]
  tags = {
    Name = "demo-app-01"
  }
}
