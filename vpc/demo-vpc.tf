variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "availability_zones" {
  type        = "list"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f"]
  description = "List of Availability Zones"
}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_vpc" "my_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags {
    Name = "DEMO"
  }
}

resource "aws_subnet" "public" {
  count                   = 6
  vpc_id                  = "${aws_vpc.my_vpc.id}"
  cidr_block              = "10.0.${count.index+1}.0/24"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true

  tags {
    Name = "DEMO Public Subnet ${count.index+1}"
  }
}

resource "aws_subnet" "private" {
  count             = 6
  vpc_id            = "${aws_vpc.my_vpc.id}"
  cidr_block        = "10.0.${count.index+11}.0/24"
  availability_zone = "${element(var.availability_zones, count.index)}"

  tags {
    Name = "DEMO Private Subnet ${count.index+1}"
  }
}

//----------------------------------------------------------------------
//        Create GW

resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = "${aws_vpc.my_vpc.id}"

  tags {
    Name = "DEMO Internet Gateway"
  }
}

//------------------------------------------------------------------------
//        Create RT for subnets

resource "aws_route_table" "rtDEMO" {
  vpc_id = "${aws_vpc.my_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my_vpc_igw.id}"
  }

  tags {
    Name = "DEMO Public Subnet Route Table"
  }
}

resource "aws_route_table_association" "public" {
  count          = 6
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.rtDEMO.id}"
}
